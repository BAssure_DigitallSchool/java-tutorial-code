
public class Sample {

    //SHARED with Git
    
    //PRIMITIVE DATA TYPES
    //byte short int long float double boolean char
    
    
    public static void main(String... args) {
        //variable-type/data-type/type variable-name
        int a;
        
        //a = "John";
        a = 25;
        
        System.out.println(a);
        
        String str = "Java Language";
        System.out.println(str);
    }
}
