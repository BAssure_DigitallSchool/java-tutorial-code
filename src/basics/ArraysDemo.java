package basics;

public class ArraysDemo {

    public static void main(String[] args) {
        int[][][][] b = new int[3][4][5][8];
        int[][] a;
        
        a = new int[4][];
        a[0] = new int[3];
        a[1] = new int[2];
        a[2] = new int[4];
        a[3] = new int[2];
        
        for(int t1[] : a){
            for(int t2: t1){
                System.out.print(t2 + " ");
            }
            System.out.println("");
        }
    }
    
    public static void main5(String[] args) {
        int[][] a = {{1, 2, 9}, {3, 4}, {5, 6, 10, 11}, {7, 8}};
        
        for(int t1[] : a){
            for(int t2: t1){
                System.out.print(t2 + " ");
            }
            System.out.println("");
        }
    }
    
    public static void main4(String[] args) {
        int k[][], m;
        int[][] b, n;
        int[] c[], p;
        
        k = new int[2][3];
        
        for(int t1[] : k){
            for(int t2: t1){
                System.out.print(t2 + " ");
            }
            System.out.println("");
        }
        
        System.out.println("#############");
        for(int i=0; i<k.length; i++){
            for(int j=0; j<k[i].length; j++){
                System.out.print(k[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void main3(String[] args) {
        int[] a = {10, 14, 12, 18, 15, 14, 16};

        System.out.println(a);
        System.out.println(a[2]);
        System.out.println(a.length);

        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

        System.out.println("");
        for (int t : a) {
            System.out.print(t + " ");
        }

//        int[] b = null;
//        System.out.println(b.length);
    }

    public static void main2(String[] args) {
        int[] a = new int[4];
        int[] b;

        b = new int[3];

        int i = 10;
        int[] m = {10, 20, 30};
    }

    public static void main1(String[] args) {
        int i, j = 15, k;
        int a[], b, c[];

        int[] m, n, p;
    }

}
