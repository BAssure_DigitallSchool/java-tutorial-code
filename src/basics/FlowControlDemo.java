package basics;

public class FlowControlDemo {

    public static void main(String[] args) {
        int a = 10, b = 8;

        int max = a > b ? a : b;
        
        System.out.println(max);

    }

    public static void main3(String[] args) {
        //marks < 40 failed, >=40 <50 third class, 
        // >=50 <60 second class, >=60 <75 first class
        // >= 75 distinction
        int marks = 45;

        if (marks < 40) {
            System.out.println("Failed");
        }

        if (marks >= 40 && marks < 50) {
            System.out.println("Third Class");
        }

        if (marks >= 50 && marks < 60) {
            System.out.println("Second Class");
        }

        if (marks >= 60 && marks < 75) {
            System.out.println("First Class");
        }

        if (marks >= 75) {
            System.out.println("Distinction");
        }

        // 2nd algorithm
        if (marks < 40) {
            System.out.println("Failed");
        } else {
            if (marks >= 40 && marks < 50) {
                System.out.println("Third Class");
            } else {
                if (marks >= 50 && marks < 60) {
                    System.out.println("Second Class");
                } else {
                    if (marks >= 60 && marks < 75) {
                        System.out.println("First Class");
                    } else {
                        System.out.println("Distinction");
                    }
                }
            }
        }

        // 3rd algorithm
        if (marks < 40) {
            System.out.println("Failed");
        } else if (marks >= 40 && marks < 50) {
            System.out.println("Third Class");
        } else if (marks >= 50 && marks < 60) {
            System.out.println("Second Class");
        } else if (marks >= 60 && marks < 75) {
            System.out.println("First Class");
        } else {
            System.out.println("Distinction");
        }

        // 4th algorithm
        if (marks >= 75) {
            System.out.println("Distinction");
        } else if (marks >= 60) {
            System.out.println("First Class");
        } else if (marks >= 50) {
            System.out.println("Second Class");
        } else if (marks >= 40) {
            System.out.println("Third Class");
        } else {
            System.out.println("Failed");
        }
    }

    public static void main2(String[] args) {
        int month = 4;

        switch (month) {
            case 2:
                System.out.println("February");
                break;
            case 1:
                System.out.println("January");
                break;
            case 3, 4, 5:
                System.out.println("It's Summer");
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            default:
                System.out.println("Not a month");
        }
    }

    public static void main1(String[] args) {
        // marks>=40 passed
        int marks = 54;

        if (marks >= 40) {
            System.out.println("Passed");
        } else {
            System.out.println("Failed");
        }
    }
}
