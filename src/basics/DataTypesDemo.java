package basics;

import java.io.BufferedInputStream;

public class DataTypesDemo {

    public static void main(String[] args) {
            
    }
    
    public static void main3(String[] args) {
        var t1 = 25;
        var t2 = 34561234567L;
        var t3 = "Hello Java World";
        
        //t2 = 12.34;
        
        BufferedInputStream in1 = new BufferedInputStream(null);
        var in2 = new BufferedInputStream(null);
    }
    
    \u0070ublic static void \u006dain2(String[] args) {
        char c = '\u0905';
        System.out.println(c);
        System.out.println((int) c);
        
        System.out.println('\u004B');
        System.out.println('K');
        
        c = 'M';
        System.out.println(c < 'B');
        System.out.println(c != 'M');
        // EQUALITY OPERATORS: == !=
        
        boolean temp = 'A' > 65;
        boolean temp2 = true == false;
    }
    
    public static void main1(String[] args) {
        //primitive type: NAME, MEMORY, VALUE-TYPE, VALUE-RANGE, & OPERATIONS
        byte b = +127;
        short s = -255;
        int i = 4567890;
        long l = 3245671893L;

        float f = 456.789F;
        double d = 3456789.45667;

        // 1 BYTE unsigned: 00000000 - 11111111
        // BIT - B(Binary) IT(Digit)
        // UTF - Unicode Text Format
        char c = 'J';

        boolean bool = true;
        
        

        System.out.println(b);
        System.out.println(b + 2);
        System.out.println(b - 2);
        System.out.println(b * 2);
        System.out.println(b / 2);
        System.out.println(b % 2);

        System.out.println(s);
        System.out.println(s - 2);
        System.out.println(s + 260);
    }
}
