package basics;

public class LogicalOperatorsDemo {

    public static void main(String[] args) {
        boolean m1 = true && 10 > 12 - 1;

        System.out.println(m1);
        
        boolean m2 = 10 < 5 || 3 > 4;
        
        boolean m3 = !(10 < 14);
        
        boolean m4 = !((10 * 12) > (10 + 50));
        
        {
            System.out.println("Hello World");
            System.out.println("Hi Again");
        }
        
        {
            System.out.println("Vanakkam");
            System.out.println("Namaste");
        }
    }

    public static void main3(String[] args) {
        // ! TRUTH TABLE
        // !false = true
        // !true = false

        boolean m1 = !false;
        boolean m2 = !true;

        System.out.println(m1);
        System.out.println(m2);
    }

    public static void main2(String[] args) {

        // || TRUTH TABLE
        // false || false = false
        // false || true = true
        // true || false = true
        // true || true = true
        boolean m1 = false || false;
        boolean m2 = false || true;
        boolean m3 = true || false;
        boolean m4 = true || true;
        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m3);
        System.out.println(m4);
    }

    public static void main1(String[] args) {
        int a = 10 + 20;
        boolean b = 10 > 20;

        // && (and), || (or), ! (not)
        // && TRUTH TABLE
        // false && false = false
        // false && true = false
        // true && false = false
        // true && true = true
        boolean m1 = false && false;
        boolean m2 = false && true;
        boolean m3 = true && false;
        boolean m4 = true && true;
        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m3);
        System.out.println(m4);
    }
}
