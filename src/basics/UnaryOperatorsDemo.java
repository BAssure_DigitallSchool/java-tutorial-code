
package basics;


public class UnaryOperatorsDemo {
    public static void main(String[] args) {
        int i = 10;
        
        System.out.println(i++);
        
        System.out.println(++i);
    }
    
    public static void main1(String[] args) {
        // ++ increment
        // -- decrement
        
        //10++; 10 = 10 + 1
        
        int i = 10;
        i++; // i = i + 1
        System.out.println(i);
        ++i;
        System.out.println(i);
    }
}
