package basics;

public class LoopsDemo {

    public static void main(String[] args) {
        int i=1;
        do{
            System.out.println("10 x " + i + " = " + 10 * i);
            i++;
        }while(i<=10);
    }
    
    public static void main2(String[] args) {
        int i = 1;
        while (i <= 10) {
            System.out.println("10 x " + i + " = " + 10 * i);
            i++;
        }

    }

    public static void main1(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println("10 x " + i + " = " + 10 * i);
        }

    }
}
